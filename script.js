function XLSExport()
{
    console.log( 'excel export function is initial');
    // ui-btn-clock
    let
        form = document.getElementById('performance'),
        gridname = form && form.querySelector('[name="gridname"]').value,
        from = form && form.querySelector('[name="from"]').value,
        to = form && form.querySelector('[name="to"]').value,
        uiToolbarContainer = document.getElementById('uiToolbarContainer'),
        uiToolbarBtnBox = uiToolbarContainer && uiToolbarContainer.querySelector('.ui-toolbar-right-buttons'),
        buttonXLS = uiToolbarBtnBox && uiToolbarBtnBox.querySelector('a.ui-btn')
    ;

    if ( !buttonXLS ) {
        console.error('Не удалось найти кнопку');
        return false;
    }

    let file = ExcellentExport.excel(buttonXLS, gridname + '_table', from + ' - ' + to + ' отчет о производительности ');
    buttonXLS.setAttribute('download', from + ' - ' + to +  '_report_performance.xls');
}
