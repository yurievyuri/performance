<?php
    set_time_limit(60);
    require($_SERVER[ 'DOCUMENT_ROOT' ]. '/bitrix/header.php' );

    \Bitrix\Main\Page\Asset::getInstance()->addJs( '/local/assets/excellentexport/dist/excellentexport.js' );
    \Bitrix\Main\Page\Asset::getInstance()->addJs( '/report/performance/script.js' );

    \Bitrix\Main\Loader::includeModule('sale');

    $namePage = 'Эффективность работы';
    $APPLICATION->SetTitle($namePage);
    $filterName = 'FILTER_PERFORMANCE';
    $gridName = 'GRID_PERFORMANCE';
    $findedUsers = [];
    $list = [];
    $filter = [];
    $xlcButton = new \Bitrix\UI\Buttons\Button([
        'link' => '#',
        'text' => 'XLS export',
        "icon" => \Bitrix\UI\Buttons\Icon::DISK,
        "color" => \Bitrix\UI\Buttons\Color::PRIMARY_DARK,
        'click' => new \Bitrix\UI\Buttons\JsHandler(
            'XLSExport' //ссылка на функцию
        )
    ]);
    $filterHeader = [
        'FILTER_ID' => $filterName,
        'GRID_ID' => $gridName,
        'FILTER' => [
            ['id' => 'DATE', 'name' => 'Дата', 'type' => 'date', 'default' => true],
            [
                'id' => 'USER_ID',
                'name' => 'Ответственный',
                'type' => 'dest_selector',
                'default' => true,
                'params' => [
                    'useNewCallback' => 'Y',
                    //'lazyLoad' => 'Y',
                    //'context' => $propName,
                    'contextCode' => 'U',
                    'enableUsers' => 'Y',
                    'userSearchArea' => 'I', // E
                    'enableSonetgroups' => 'N',
                    'enableDepartments' => 'Y',
                    'allowAddSocNetGroup' => 'N',
                    'departmentSelectDisable' => 'N',
                    'showVacations' => 'Y',
                    'duplicate' => 'N',
                    'enableAll' => 'N',
                    'departmentFlatEnable' => 'Y',
                    'useSearch' => 'N',
                    'multiple' => 'Y',
                    'enableEmpty' => 'N'
                ]
            ],
            [
                'id' => 'SOURCE_ID',
                'name' => 'Учитывать источник лида',
                'type' => 'list',
                'default' => true,
                //'partial' => true,
                'items' =>  CCrmStatus::getStatusList('SOURCE'),
                'params' => [
                    'multiple' => 'Y'
                ],
                //'value'
            ]
        ],
        'ENABLE_LIVE_SEARCH' => 'N',
        'ENABLE_LABEL' => 'N',
        'THEME' => 'BORDER'
    ];

    \Bitrix\UI\Toolbar\Facade\Toolbar::addButton( $xlcButton );
    \Bitrix\UI\Toolbar\Facade\Toolbar::addFilter($filterHeader);

    // Получаем данные для фильтрации.
    $filterOptions = new \Bitrix\Main\UI\Filter\Options( $filterName );
    //$filterData = $filterOptions->getPresets();

    /*if( $filterData['SOURCE_ID'])
    array (
        0 => 'FACE_TRACKER',
        1 => 'WEB',
        2 => '5',
    )*/

    $filterFields = $filterOptions->getFilter();
    if ( !$filterFields['USER_ID'] )
    {
        $filterFields['USER_ID'][0] = 'DR56';
        $filterFields['USER_ID_label'][0] = 'Отдел по работе с клиентами';
    }
    foreach ( $filterFields['USER_ID'] as $key => $userId )
    {
        $recursive = false;
        $tempGroupId = false;

        // просто пользователи
        if ( stristr($userId, 'U') )
        {
            $filterFields[ 'USER_ID' ][ $key ] = str_ireplace( 'U', '', $userId );
            $filterFields[ 'USER_ID_label' ][ $filterFields[ 'USER_ID' ][ $key ] ] = $filterFields[ 'USER_ID_label' ][ $key ];
            unset($filterFields[ 'USER_ID_label' ][ $key ]);
            continue;
        }

        // группа с подгруппами
        if ( stristr($userId, 'DR') )
        {
            $tempGroupId = str_ireplace( 'DR', '', $userId );
            $recursive = true;
        }
        // группа
        if ( stristr($userId, 'R') && !stristr($userId, 'DR') )
            $tempGroupId = str_ireplace( 'R', '', $userId );

        if ( !$tempGroupId ) continue;

        $db = CIntranetUtils::getDepartmentEmployees($tempGroupId, $recursive, false, false);
        while ( $list = $db->Fetch() )
        {
            if ( !in_array($list['ID'],$filterFields[ 'USER_ID' ]) )
            {
                $filterFields[ 'USER_ID' ][] = $list['ID'];
                $filterFields[ 'USER_ID_label' ][ $list['ID'] ] = $list['NAME'].' '.$list['LAST_NAME'];
            }
        }
        unset( $filterFields[ 'USER_ID' ][ $key ], $filterFields['USER_ID_label'][ $key ]);
    }

    if ( $filterFields['DATE_from'] )
        $filter['>=DATE_CREATE'] = $filterFields['DATE_from'];
    if ( $filterFields['DATE_to'] )
        $filter['<=DATE_CREATE'] = $filterFields['DATE_to'];

    asort($filterFields['USER_ID_label']);

    // собираем грид
    $grid_options = new \Bitrix\Main\Grid\Options($gridName);
    //$visible = $grid_options->GetVisibleColumns();
    $sort = $grid_options->GetSorting(['sort' => ['user' => 'ASC'], 'vars'=>array('by'=>'by', 'order'=>'order')  ]);
    $nav_params = $grid_options->GetNavParams();
    $nav = new Bitrix\Main\UI\PageNavigation($filterName);
    $nav->allowAllRecords(true)
            ->setPageSize($nav_params['nPageSize'])
                ->initFromUri();

    // собираем ROWS для грида
    $up = new \Dev\Utils\UserPerformance( $filter, $filterFields );

    foreach ( $filterFields['USER_ID_label'] as $userId => $name )
    {
        // подключение столбцов
        foreach ( $up::calcFunction as $enName => $ruName ) //if ( in_array( $enName,$visible ) )
            $list[ $userId ][ 'data' ][ $enName ] = $up->{$enName}( $userId );

        // подключение столбцов из фильтра по источнику обращения
        foreach ( $up->getSourceColumns() as $sourceId => $ruName )
            $list[ $userId ][ 'data' ][ $sourceId ] = $up->invoiceCalcBySourceV2( $sourceId, $userId );
    }

    $APPLICATION->IncludeComponent(
        'bitrix:main.ui.grid',
        '',
        [
            //'FILTER' => $filter,
            //'MESSAGES' => ['error' => ['title'=>'dsfsdfsdf']],
            'GRID_ID'       => $gridName,
            'COLUMNS'       => $up->getColumns(),
            'ROWS'          => $list,
            'NAV_OBJECT'    => $nav,
            'AJAX_MODE'     => 'Y',
            'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
            /*'PAGE_SIZES' => [
                ['NAME' => '5', 'VALUE' => '5'],
                ['NAME' => '10', 'VALUE' => '10'],
                ['NAME' => '25', 'VALUE' => '25'],
            ],*/
            'SHOW_ALWAYS'               =>  true,
            'SHOW_ROW_CHECKBOXES'       =>  false,
            'AJAX_OPTION_JUMP'          =>  'N',
            'SHOW_CHECK_ALL_CHECKBOXES' => false,
            'SHOW_ROW_ACTIONS_MENU'     => false,
            'SHOW_GRID_SETTINGS_MENU'   => true,
            'SHOW_NAVIGATION_PANEL'     => true,
            'SHOW_PAGINATION'           => false,
            'SHOW_SELECTED_COUNTER'     => false,
            'SHOW_TOTAL_COUNTER'        => false,
            'SHOW_PAGESIZE'             => false,
            'SHOW_ACTION_PANEL'         => false,
            'ALLOW_COLUMNS_SORT'        => true,
            'ALLOW_COLUMNS_RESIZE'      => true,
            'ALLOW_HORIZONTAL_SCROLL'   => true,
            'ALLOW_SORT'                => true,
            'ALLOW_PIN_HEADER'          => true,
            'AJAX_OPTION_HISTORY'       => 'N'
        ]
    );
?>
<form id="performance">
    <input type="hidden" name="gridname" value="<?=$gridName?>">
    <input type="hidden" name="from" value="<?=date('d.m.Y', strtotime($up->from));?>">
    <input type="hidden" name="to" value="<?=date('d.m.Y', strtotime($up->to));?>">
</form>


<?require($_SERVER[ 'DOCUMENT_ROOT' ]. '/bitrix/footer.php' );